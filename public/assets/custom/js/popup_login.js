/**
 * Javascript général
 */

jQuery(function() {

  //alert("test");
  jQuery('#login-submit').click(function (e) {
    e.preventDefault();
    alert("test");
    var email = jQuery('#user_email').val();
    var password = jQuery('#user_password').val();
    var url_redirect = jQuery(this).data('urlredirect');
    var data_send = JSON.stringify({email: email, password: password});
    console.log(data_send);
    jQuery.ajax({
      url: url_login,
      type: 'POST',
      contentType: "application/json",
      dataType: 'json',
      data: data_send,
      success: function (data, status) {
        console.log("DATA", data);
        jQuery('#gn-loader').hide();
        if (data.user > 0)
        {
          window.location.reload();
        }
      },
      beforeSend: function(){
        jQuery('#gn-loader').show();
      },
      error: function(data, status, object) {
        jQuery("#content-msg-outer").show();
        jQuery('#content-msg').text('Identifiants invalides');
        jQuery('#gn-loader').hide();
      }
    });
  });
});
