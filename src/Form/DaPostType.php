<?php

namespace App\Form;

use App\Entity\DaPost;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DaPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('postTitle', TextType::class, [
                'attr' => ['class' => 'form-control postTitle'],
                "label" => "Titre de l'article"
            ])
            ->add('postContent', CKEditorType::class, array(
                'config' => array('toolbar' => 'full'),
                "label" => "Contenu de l'article"
            ))
            ->add('categories',  EntityType::class, array(
                'label'         => 'Catégorie',
                'class'         => 'App\Entity\DaPostCategory',
                'query_builder' => function (EntityRepository $_er) {
                    return $_er
                        ->createQueryBuilder('ctg')
                        ->orderBy('ctg.id', 'DESC');
                },
                'choice_label'  => 'categoryTitle',
                'multiple'      => true,
                'expanded'      => false,
                'attr' => ['class' => 'form-control categorie-select', 'data-placeholder' => '- Séléctionner une ou plusieurs catégories ici-'],
                'required'      => true,
                'placeholder'   => '- Séléctionner les catégories -'
            ))
            ->add('metaKey', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('postUrl', TextType::class, [
                'attr' => ['class' => 'form-control postUrl', 'required' => true],
                "label" => "URL de l'article (automatique)"
            ])
            ->add('metaDescription', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image à la une',
                'required'  => false,
                'allow_delete'  => true,
                'download_label'  => 'Télécharger',
                'download_uri'  => true,
                'image_uri'  => true,
                'imagine_pattern'  => 'thumbnail_list',
                'asset_helper'  => true,
                'attr' => [
                    'class' => 'form-control mb-3 imageFileClass',
                    'style' => "border:none!important"
                ]
            ])
            ->add('videoFile', VichFileType::class, [
                'label' => "Video de l'article",
                'required'  => false,
                'allow_delete'  => true,
                'download_label'  => 'Télécharger',
                'download_uri'  => true,
                'asset_helper'  => true,
                'attr' => [
                    'class' => 'form-control mb-3 videoFileClass',
                    'style' => "border:none!important"
                ]
            ]);

            if($options['allow_user_create_published_at_for_post']) {
                $builder
                    ->add('postPublishedAt', DateTimeType::class, [
                        'attr' => ['class' => 'form-control postCreatedAt'],
                        'required'  => false,
                        "label" => "À publier le : ",
                        'widget' => 'single_text',
                    ]);
            }
            
            if($options['allow_user_create_published_at_for_post']) {
                $builder
                    ->add('postPublishedAt', DateTimeType::class, [
                        'attr' => ['class' => 'form-control postCreatedAt'],
                        'required'  => false,
                        "label" => "À publier le : ",
                        'widget' => 'single_text',
                    ]);
            }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DaPost::class,
            "allow_user_create_published_at_for_post" => false,
            "allow_user_update_published_at_for_post" => false,
            
        ]);
    }
}
