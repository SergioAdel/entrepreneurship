<?php

namespace App\Controller;

use App\Entity\DaPostCategory;
use App\Repository\DaPostCategoryRepository;
use App\Repository\DaPostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category/{postCategoryUrl}", name="app_da_post_category_detail")
    */

    public function show($postCategoryUrl, PaginatorInterface $paginatorInterface, Request $request, DaPostCategoryRepository $daPostCategoryRepository): Response
    {
        $daPostCategory = $daPostCategoryRepository->findOneByPostCategoryUrl($postCategoryUrl);
        if(!$daPostCategory) {
            return $this->redirectToRoute('app_da_home');
        } 
        $posts = $paginatorInterface->paginate(
            $daPostCategory->getDaPosts(),
            $request->query->get("page", 1),
            10
        );
        return $this->render('front/category/show.html.twig', [
           "category" => $daPostCategory,
           "posts" => $posts
        ]);
    }
}
