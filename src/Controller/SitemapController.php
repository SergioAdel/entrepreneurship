<?php

namespace App\Controller;

use App\Repository\DaPostCategoryRepository;
use App\Repository\DaPostRepository;
use App\Repository\DaUserRepository;
use App\Service\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    #[Route('/sitemap.xml', name: 'app_da_sitemap', defaults: ["_format" => "xml"])]
    public function index(
        Request $request,
        DaPostRepository $daPostRepository,
        DaPostCategoryRepository $daPostCategoryRepository,
        DaUserRepository $daUserRepository,
        Post $post
    ): Response {


        $hostname = $request->getSchemeAndHttpHost();
        $urls = [];
        $urls[] = ['loc' => $this->generateUrl('app_da_home')];


        foreach ($daPostRepository->findAll() as $daPost) {
            $urls[] = [
                'loc' => $this->generateUrl('app_da_post_detail', [
                    'postUrl' => $daPost->getPostUrl()
                ]),
                'lastmod' => $daPost->getPostCreatedAt()->format('Y-m-d')
            ];
            $urls[] = [
                'loc' => $this->generateUrl('app_da_post_detail_readMore', [
                    'postUrl' => $daPost->getPostUrl(),
                    'year' => $daPost->getPostCreatedAt()->format('Y'),
                    'month' => $daPost->getPostCreatedAt()->format('m'),
                    'day' => $daPost->getPostCreatedAt()->format('d')
                ]),
                'lastmod' => $daPost->getPostCreatedAt()->format('Y-m-d')
            ];
        }


        foreach ($daUserRepository->findAll() as $users) {
            if (in_array('ROLE_JOURNALIST', $users->getRoles())) {
                $urls[] = [
                    'loc' => $this->generateUrl('app_da_user_post', [
                        'userName' => $users->getUserName()
                    ]),
                    'lastmod' => $users->getCreatedAt()->format('Y-m-d')
                ];
            }
        }


        foreach ($post->getAllDateAvalableArchive() as $archives) {
            $dateYM = explode(' ', $archives);
            $urls[] = [
                'loc' => $this->generateUrl('app_da_archive_post', [
                    'year' => $dateYM[0],
                    'month' => $dateYM[1]
                ]),
                'lastmod' => $dateYM[0] . '-' . $dateYM[1] . '-' . '1'
            ];
        }


        foreach ($daPostCategoryRepository->findAll() as $categories) {
            $urls[] = ['loc' => $this->generateUrl('app_da_post_category_detail', [
                'postCategoryUrl' => $categories->getPostCategoryUrl()
            ])];
        }

        $response = new Response(
            $this->renderView(
                'sitemap/index.html.twig',
                [
                    'urls' => $urls,
                    'hostname' => $hostname
                ],
                Response::HTTP_OK
            )
        );

        $response->headers->set('content-type', 'text/xml');

        return $response;
    }
}
