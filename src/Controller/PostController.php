<?php

namespace App\Controller;

use App\Entity\DaPost;
use App\Entity\DaUser;
use App\Repository\DaPostRepository;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\DaPostCategoryRepository;
use App\Service\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PostController extends AbstractController
{
    /**
     * @Route("/", name="app_da_home")
     */
    public function index(DaPostRepository $daPostRepository, Request $request, DaPostCategoryRepository $daPostCategoryRepository, Post $post): Response
    {
        $enContinus = $daPostRepository->getEnContinu(12);
        $alaunes = $daPostRepository->get('investir-en-afrique', 6);
        $famous = $daPostRepository->getFamous();
        $lastSport = $daPostRepository->getLastSport();
        $politiques = $daPostRepository->get('entreprendre', 6);
        $coronavirus = $daPostRepository->get('financement', 3);
        $economies = $daPostRepository->get('portrait', 5);
        $divers = $daPostRepository->get('juridique-et-fiscalite', 5);
        $sts = $daPostRepository->get('startup', 9);
        $societes = $daPostRepository->get('gestion-d-entreprise', 6);
        $sport = $daPostRepository->get('investir-en-afrique')[0];
        return $this->render('front/post/home.html.twig', [
            'enContinus' => $enContinus,
            'alaunes' => $alaunes,
            'famous' => $famous,
            'lastSport' => $lastSport,
            'politiques' => $politiques,
            'coronavirus' => $coronavirus,
            'economies' => $economies,
            'divers' => $divers,
            'sts' => $sts,
            'societes' => $societes,
            'sport' => $sport,
        ]);
    }

    /**
     * @Route("/post/{postUrl}", name="app_da_post_detail")
     */

    public function show(DaPost $daPost): Response
    {
        if (!$daPost) {
            return $this->createNotFoundException("Actualités non trouvé");
        }
        $postCreatedAt = $daPost->getPostCreatedAt();
        $year = $postCreatedAt->format('Y');
        $month = $postCreatedAt->format('m');
        $day = $postCreatedAt->format('d');
        return $this->redirectToRoute('app_da_post_detail_readMore', [
            'year' => $year,
            'month' => $month,
            'day' => $day,
            'postUrl' => $daPost->getPostUrl()
        ]);
    }

    /**
     * @Route("/{year}/{month}/{day}/{postUrl}", name="app_da_post_detail_readMore")
     */

    public function readMore(DaPost $daPost, DaPostRepository $daPostRepository): Response
    {
        if (!$daPost) {
            return $this->createNotFoundException("Actualités non trouvé");
        }

        $next = $daPostRepository->findOneById($daPost->getId() + 1);
        $prev = $daPostRepository->findOneById($daPost->getId() - 1);
        $more = $daPostRepository->findBy([], null, 3);

        return $this->render('front/post/show.html.twig', [
            'postDetail' => $daPost,
            'prev' => $prev,
            'next' => $next,
            'more' => $more,
        ]);
    }
    /**
     * @Route("/post/{postUrl}/comment", name="app_da_post_comment")
     */

    public function comment(DaPost $daPost): Response
    {
        if (!$daPost) {
            return $this->createNotFoundException("Actualités non trouvé");
        }
        $postCreatedAt = $daPost->getPostCreatedAt();
        $year = $postCreatedAt->format('Y');
        $month = $postCreatedAt->format('m');
        $day = $postCreatedAt->format('d');
        return $this->redirectToRoute('app_da_post_detail_readMore', [
            'year' => $year,
            'month' => $month,
            'day' => $day,
            'postUrl' => $daPost->getPostUrl()
        ]);
    }

    /**
     * @Route("/post/author/{userName}", name="app_da_user_post")
     */

    public function userPost(DaUser $daUser, DaPostRepository $daPostRepository, PaginatorInterface $paginatorInterface, Request $request): Response
    {
        if (!$daUser) {
            return $this->createNotFoundException("Utilisateur non trouvé");
        }
        $posts = $paginatorInterface->paginate(
            $daPostRepository->findByUser($daUser, ['postCreatedAt' => 'DESC']),
            $request->query->get("page", 1),
            10
        );
        return $this->render('front/post/journalist.html.twig', [
            'daUser' => $daUser,
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/{year}-{month}", name="app_da_archive_post")
     */

    public function archive($year, $month, DaPostRepository $daPostRepository, PaginatorInterface $paginatorInterface, Request $request): Response
    {
        $postss = $daPostRepository->getByYM($year, $month);
        if (!$postss || empty($postss))
            return $this->redirectToRoute("app_da_home");
        $posts = $paginatorInterface->paginate(
            $daPostRepository->getByYM($year, $month),
            $request->query->get("page", 1),
            10
        );
        return $this->render('front/post/archive.html.twig', [
            'posts' => $posts,
            'archiveDate' => $year . " " . $month
        ]);
    }
}
