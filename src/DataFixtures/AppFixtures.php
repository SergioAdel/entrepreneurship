<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\DaPost;
use App\Entity\DaRole;
use App\Entity\DaUser;
use App\Entity\DaCountry;
use App\Entity\DaPostCategory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasherEncoder;
    private $sluggerInterface;
    public function __construct(UserPasswordHasherInterface $passwordHasherEncoder, SluggerInterface $sluggerInterface)
    {
        $this->passwordHasherEncoder = $passwordHasherEncoder;
        $this->sluggerInterface = $sluggerInterface;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        // Les Roles
        $roles[] = [];
        $tabRoles = ["ROLE_USER", "ROLE_ADMIN", "ROLE_JOURNALIST", "ROLE_ABONNE_SIMPLE", "ROLE_ABONNE_PREMIUM", "ROLE_ABONNE_VIP"];
        for ($i = 0; $i < 6; $i++) {
            $role = new DaRole();
            $role->setRoleName($tabRoles[$i]);
            $role->setRoleDescription($faker->text(100));
            $manager->persist($role);
            $roles[] = $role;
        }

        // Utilisateur
        $users[] = [];
        for ($i = 0; $i < 30; $i++) {
            $user = new DaUser();
            $user->setUserName($faker->userName);
            $user->setUserFullname($faker->firstName . " " . $faker->lastName);
            $user->setUserPassword($this->passwordHasherEncoder->hashPassword($user, "password123"));
            $user->setCreatedAt(new \DateTime());
            $user->setConfirmAt(new \DateTime());
            $user->setUpdateAt(new \DateTime());
            $user->setUserEmail($faker->email);
            $user->setIsDeleted(0);
            $user->addDaRole($roles[rand(1, 6)]);

            $manager->persist($user);
            $users[] = $user;
        }

        // Les pays
        $countries[] = [];
        for ($i = 0; $i < 10; $i++) {
            $country = new DaCountry();
            $country->setCountryName($faker->country);
            $country->setCountryIsocode('ISO ' . $i);
            $country->setImageFlag("image-default.jpg");
            $country->setUpdatedAt(new \DateTime());
            $manager->persist($country);
            $countries[] = $country;
        }

        // Les catégories
        $categories[] = [];
        $tabCat = ["Investir en afrique", "Entreprendre", "Financement", "Gestion d'entreprise", "Jusridiques et Fiscalité", "Startup", "Portrait"];
        for ($i = 0; $i < count($tabCat); $i++) {
            $category = new DaPostCategory();
            $category->setCategoryTitle($tabCat[$i]);
            $category->setCategoryDescription($faker->paragraph(4));
            $category->setIsDeleted(false);
            $category->setMetaDescription("Meta " . $i);
            $category->setPostCategoryPhotos("image-default.jpg");
            $category->setPostCategoryUrl(strtolower($this->sluggerInterface->slug($category->getCategoryTitle())));

            $manager->persist($category);
            $categories[] = $category;
        }

        // Les postes
        $posts[] = [];
        for ($i = 0; $i < 250; $i++) {
            $post = new DaPost();
            $post->setUser($users[rand(1, 30)]);
            $post->setPostContent($faker->paragraph(25));
            $post->setPostTitle($faker->paragraph(1));
            $post->setPostCreatedAt(new \DateTime());
            $post->setUpdatedAt(new \DateTime());
            $post->setPostPhotos("image-default.jpg");
            $post->setPostVideo("video.mp4");
            $post->setIsCommented(0);
            $post->setIsApprouved(1);
            $post->setIsDeleted(0);
            $post->setMetaKey("meta key" . $i);
            $post->setMetaDescription("meta description" . $i);
            $post->setPostUrl(strtolower($this->sluggerInterface->slug($post->getPostTitle())));
            $post->addCategory($categories[rand(1, count($tabCat))]);
            $manager->persist($post);
            $posts[] = $post;
        }

        $manager->flush();
    }
}
