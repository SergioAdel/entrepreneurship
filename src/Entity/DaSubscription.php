<?php

namespace App\Entity;

use App\Repository\DaSubscriptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DaSubscriptionRepository::class)
 * @ORM\Table(name="da_subscriptions")
 */
class DaSubscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $subscriptionName;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $subscriptionPrice;

    /**
     * @ORM\OneToMany(targetEntity=DaUser::class, mappedBy="daSubscription")
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $SubscriptionDescription;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $SubscriptionDelay;

    /**
     * @ORM\ManyToOne(targetEntity=DaRole::class)
     */
    private $subscriptionRole;

 

    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubscriptionName(): ?string
    {
        return $this->subscriptionName;
    }

    public function setSubscriptionName(string $subscriptionName): self
    {
        $this->subscriptionName = $subscriptionName;

        return $this;
    }

    public function getSubscriptionPrice(): ?float
    {
        return $this->subscriptionPrice;
    }

    public function setSubscriptionPrice(?float $subscriptionPrice): self
    {
        $this->subscriptionPrice = $subscriptionPrice;

        return $this;
    }

    /**
     * @return Collection|DaUser[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(DaUser $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->setDaSubscription($this);
        }

        return $this;
    }

    public function removeUser(DaUser $user): self
    {
        if ($this->user->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getDaSubscription() === $this) {
                $user->setDaSubscription(null);
            }
        }

        return $this;
    }

    public function getSubscriptionDescription(): ?string
    {
        return $this->SubscriptionDescription;
    }

    public function setSubscriptionDescription(?string $SubscriptionDescription): self
    {
        $this->SubscriptionDescription = $SubscriptionDescription;

        return $this;
    }

    public function getSubscriptionDelay(): ?int
    {
        return $this->SubscriptionDelay;
    }

    public function setSubscriptionDelay(?int $SubscriptionDelay): self
    {
        $this->SubscriptionDelay = $SubscriptionDelay;

        return $this;
    }

    public function getSubscriptionRole(): ?DaRole
    {
        return $this->subscriptionRole;
    }

    public function setSubscriptionRole(?DaRole $subscriptionRole): self
    {
        $this->subscriptionRole = $subscriptionRole;

        return $this;
    }
}