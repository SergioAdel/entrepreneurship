<?php

namespace App\Repository;

use App\Entity\DaPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DaPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method DaPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method DaPage[]    findAll()
 * @method DaPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DaPageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DaPage::class);
    }

    // /**
    //  * @return DaPage[] Returns an array of DaPage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DaPage
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}