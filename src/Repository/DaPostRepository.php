<?php

namespace App\Repository;

use App\Entity\DaPost;
use App\Entity\DaPostCategory;
use App\Service\MetierManagerBundle\Utils\EntityName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method DaPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method DaPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method DaPost[]    findAll()
 * @method DaPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DaPostRepository extends ServiceEntityRepository
{
    protected $paginator;
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, DaPost::class);
        $this->paginator = $paginator;
    }

    /**
     * @return DaPost[]
     */
    public function getPostNotApprouved()
    {
        return $this->createQueryBuilder('dap')
            ->andWhere('dap.isApprouved = 0 AND dap.isDeleted = 0')
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return DaPost[]
     */
    public function getLatest($number = 10)
    {
        return $this->createQueryBuilder('dap')
        ->andWhere('dap.isApprouved = 1 AND dap.isDeleted = 0')
        ->orderBy('dap.postCreatedAt', 'DESC')
        ->getQuery()
        ->getResult();
    }


    /**
     * @return DaPost[]
     */
    public function search($word)
    {
        return $this->createQueryBuilder('dap')
            ->andWhere('dap.isApprouved = 1 AND dap.isDeleted = 0')
            ->andWhere("dap.postTitle LIKE '%" . $word . "%' OR dap.postContent LIKE '%" . $word . "%'")
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return DaPost[]
     */
    public function getByYM($year, $month)
    {
        return $this->createQueryBuilder('dap')
            ->andWhere('dap.isApprouved = 1 AND dap.isDeleted = 0')
            ->andWhere('YEAR(dap.postCreatedAt) = :year AND MONTH(dap.postCreatedAt) = :month')
            ->setParameter('year', $year)
            ->setParameter('month', $month)
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return DaPost[]
     */
    public function getEnContinu()
    {
        return $this->createQueryBuilder('dap')
            ->andWhere('dap.isApprouved = 1 AND dap.isDeleted = 0')
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }


    public function get($category, $number = 1)
    {
        return $this->createQueryBuilder('dap')
            ->innerJoin('dap.categories', 'dapc', 'WITH', 'dapc.postCategoryUrl = :category')
            ->setParameter('category', $category)
            ->andWhere('dap.isApprouved = 1 AND dap.isDeleted = 0')
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->setMaxResults($number)
            ->getQuery()
            ->getResult();
    }


    /**
     * @return DaPost[]
     */
    public function getFamous()
    {
        return $this->createQueryBuilder('dap')
            ->andWhere('dap.isApprouved = 1 AND dap.isDeleted = 0')
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();
    }


    /**
     * @return DaPost[]
     */
    public function getLastSport()
    {
        return $this->createQueryBuilder('dap')
            ->innerJoin('dap.categories', 'dapc', 'WITH', 'dapc.postCategoryUrl = \'sport\'')
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return DaPost[]
     */
    public function getFlashNews()
    {
        return $this->createQueryBuilder('dap')
            ->andWhere('dap.isApprouved = 1 AND dap.isDeleted = 0')
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return DaPost[]
     */
    public function getArchives()
    {
        return $this->createQueryBuilder('dap')
            ->andWhere('dap.isApprouved = 1 AND dap.isDeleted = 0')
            ->orderBy('dap.postCreatedAt', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }
}
