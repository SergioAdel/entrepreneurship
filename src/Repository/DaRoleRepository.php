<?php

namespace App\Repository;

use App\Entity\DaRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DaRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method DaRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method DaRole[]    findAll()
 * @method DaRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DaRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DaRole::class);
    }

    // /**
    //  * @return DaRole[] Returns an array of DaRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DaRole
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}