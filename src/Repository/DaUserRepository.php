<?php

namespace App\Repository;

use App\Entity\DaUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DaUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method DaUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method DaUser[]    findAll()
 * @method DaUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DaUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DaUser::class);
    }
    
    /**
     * Vérifier users confirmAt not null
     * @param int $id
     * @return bool
     */
    public function getOneUserNotAbonne($id)
    {
        $bool = false;
        $user = DaUser::class;
        $users = $this->getEntityManager()->createQuery(
            "SELECT u
            FROM $user u
            LEFT JOIN u.daRoles r
            WHERE u.id = :user_id AND u.confirmAt IS NOT NULL AND u.daSubscription IS NOT NULL"
        );
        $users->setParameter('user_id', $id);
        

        
        if($users->getResult()){
            $bool = true;
        }
        return $bool;
  
        
    }

    /**
     * Vérifier users confirmAt not null
     * @param string $identifiant
     * @return array
     */
    public function findByLoginOrEmail($identifiant)
    {
        $user = DaUser::class;
        $query = $this->getEntityManager()->createQuery(
            "SELECT u
            FROM $user u
            LEFT JOIN u.daRoles r
            WHERE (u.userEmail = :identifiant  OR u.userName = :identifiant) AND u.confirmAt IS NOT NULL"
        );
        $query->setParameter('identifiant', $identifiant);
        return (is_array($query->getResult()) && !empty($query->getResult())) ? $query->getResult()[0] : [];
    }

    /**
     * Vérifier users confirmAt not null
     * @param int $id
     * @return bool
     */
    public function getOneAdmin($id)
    {
        $bool = false;
        $user = DaUser::class;
        $users = $this->getEntityManager()->createQuery(
            "SELECT u
            FROM $user u
            LEFT JOIN u.daRoles r
            WHERE u.id = :user_id AND r.id = :role_id"
        );
        $users->setParameter('user_id', $id);
        $users->setParameter('role_id', 2);

        if($users->getResult()){
            $bool = true;
        }
        return $bool;
  
        
    }

    /**
     * Vérifier users confirmAt not null
     * @return user
     */
    public function getJournalist()
    {

        $user = DaUser::class;
        $users = $this->getEntityManager()->createQuery(
            "SELECT u
            FROM $user u
            LEFT JOIN u.daRoles r
            WHERE r.id = :role_id"
        );
        $users->setParameter('role_id', 3);

        return  $users->getResult();

    }

    
    // /**
    //  * @return DaUser[] Returns an array of DaUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DaUser
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}