<?php

namespace App\Repository;

use App\Entity\DaPostCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DaPostCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method DaPostCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method DaPostCategory[]    findAll()
 * @method DaPostCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DaPostCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DaPostCategory::class);
    }

    // /**
    //  * @return DaPostCategory[] Returns an array of DaPostCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DaPostCategory
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}