<?php

namespace App\Service\MetierManagerBundle\Utils;

/**
 * Class ServiceName
 * Classe qui contient les noms constante des services (métier)
 */
class ServiceName
{
    const SRV_METIER_COUNTRY = 'da.manager.country';
    const SRV_METIER_POST = 'da.manager.post';
    const SRV_METIER_CATEGORY = 'da.manager.category';
    const SRV_METIER_ACCOUNT = 'da.manager.account';
    const SRV_METIER_UTILS = 'da.manager.utils';
    const SRV_METIER_USER_UPLOAD = 'da.manager.user.upload';
}
